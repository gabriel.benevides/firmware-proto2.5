% salvus_calib_model.m
% Finding best model for the flow sensor
% Author: Cristóvão Rufino <czr@cesar.org.br>

1;
pkg load optim

function lse = model_ax_b(p, x, y)
	a = p(1);
	b = p(2);
	y_trial = a*x.^b;
	d = y - y_trial;
	lse = sum(d.^2);
endfunction

function lse = model_asqrt_x_b(p, x, y)
	a = p(1);
	b = p(2);
	y_trial = a*sqrt(x)+b;
	d = y - y_trial;
	lse = sum(d.^2);
endfunction

function lse = model_asqrt_x(p, x, y)
	a = p(1);
	y_trial = a*sqrt(x);
	d = y - y_trial;
	lse = sum(d.^2);
endfunction

function lse = model_a_sqrtx_b_c(p, x, y)
	a = p(1);
	b = p(2);
	c = p(3);
	y_trial = a*sqrt(x + b) + c;
	d = y - y_trial;
	lse = sum(d.^2);
endfunction

data = csvread('adc_lpm.csv');
y = data(:,3);
x = data(:,2);

% First model: a*sqrt(x)
p0 = [1.0];
options = [];
p_m1 = fmins('model_asqrt_x', p0, options, [], x, y);
yl_m1 = sprintf("f(x) = %f*sqrtf(x)", p_m1(1));
yf_m1 = p_m1(1)*sqrt(x);

% Second model: a*sqrt(x) + b
p0 = [1.0 1.0];
options = [];
p_m2 = fmins('model_asqrt_x_b', p0, options, [], x, y);
yl_m2 = sprintf("f(x) = %f*sqrtf(x)+%f", p_m2(1), p_m2(2));
yf_m2 = p_m2(1)*sqrt(x)+p_m2(2);

% Third model: a*sqrt(x + b) + c
p0 = [1.0 1.0 1.0];
options = [];
p_m3 = fmins('model_a_sqrtx_b_c', p0, options, [], x, y);
yl_m3 = sprintf("f(x) = %f*sqrtf(x + %f) + %f", p_m3(1), p_m3(2), p_m3(3));
yf_m3 = p_m3(1)*sqrt(x + p_m3(2)) + p_m3(3);


% Fourth model: a*x^b
p0 = [1.0 1.0];
options = [];
p_m4 = fmins('model_ax_b', p0, options, [], x, y);
yl_m4 = sprintf("f(x) = %f*powl(x,%f)", p_m4(1), p_m4(2));
yf_m4 =  p_m4(1)*x.^p_m4(2);

plot(x, y, 'ko', x,yf_m1, 'r-', x, yf_m2, 'g-', x, yf_m3, 'b-', x, yf_m4, 'c-');
legend('Data', yl_m1, yl_m2, yl_m3, yl_m4);
xlabel('LSB');
ylabel('f(x)');

printf("Model 1: a*sqrt(x): LSE = %f\n", sum((y-yf_m1).^2));
printf("Model 2: a*sqrt(x) + b: LSE = %f\n", sum((y-yf_m2).^2));
printf("Model 3: a*sqrt(x+b) + c: LSE = %f\n", sum((y - yf_m3).^2));
printf("Model 4: a*x^b: LSE = %f\n", sum((y-yf_m4).^2));

% One code to plot 'em all!
clf;
subplot(2,2,1); plot(x, y, 'k-o', x, yf_m1, 'r-+'); xlabel('ADC LSB'); ylabel('l/min'); ylim([0 12]); title('Dados x Modelo 1'); legend('Dado', 'Modelo 1', 'location', 'southeast'); grid on;
subplot(2,2,2); plot(x, y, 'k-o', x, yf_m2, 'r-+'); xlabel('ADC LSB'); ylabel('l/min'); ylim([0 12]); title('Dados x Modelo 2'); legend('Dado', 'Modelo 2', 'location', 'southeast'); grid on;
subplot(2,2,3); plot(x, y, 'k-o', x, yf_m3, 'r-+'); xlabel('ADC LSB'); ylabel('l/min'); ylim([0 12]); title('Dados x Modelo 3'); legend('Dado', 'Modelo 3', 'location', 'southeast'); grid on;
subplot(2,2,4); plot(x, y, 'k-o', x, yf_m4, 'r-+'); xlabel('ADC LSB'); ylabel('l/min'); ylim([0 12]); title('Dados x Modelo 4'); legend('Dado', 'Modelo 4', 'location', 'southeast'); grid on;
print ('graficos.png');

% Modelo junto
yf_final(1:3) = yf_m3(1:3);
yf_final(4:11) = yf_m2(4:11);
clf;
plot(x, y, 'k-o', x, yf_final, 'r-+'); xlabel('ADC LSB'); ylabel('l/min'); ylim([0 12]); title('Dados x Modelo final'); legend('Dado', 'Modelo final', 'location', 'southeast'); grid on;
print('modelo-final.png');
