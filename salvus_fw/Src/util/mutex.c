/*
 * mutex.c
 *
 *  Created on: Dec 4, 2017
 *      Author: czr@cesar.org.br
 */

#include "util/mutex.h"
#include "stm32l0xx.h"

void mutex_lock(mutex_t *mutex)
{
	while(mutex_try_lock(mutex) == MUTEX_ERROR_LOCK);
}

void mutex_free(mutex_t *mutex)
{
	__disable_irq();
	mutex->lock = MUTEX_FREE;
	__DSB();
	__enable_irq();
}

mutex_status_e mutex_try_lock(mutex_t *mutex)
{
	mutex_status_e ret = MUTEX_ERROR_LOCK;

	__disable_irq();
	if(mutex->lock == MUTEX_FREE) {
		mutex->lock = MUTEX_LOCK;
		ret = MUTEX_OK;
		__DSB();
	}
	__enable_irq();

	return ret;
}

void mutex_init(mutex_t *mutex)
{
	__disable_irq();
	mutex->lock = MUTEX_FREE;
	__DSB();
	__enable_irq();
}
