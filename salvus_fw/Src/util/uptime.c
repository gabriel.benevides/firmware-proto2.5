/*
 * uptime.c
 *
 *  Created on: Feb 16, 2018
 *      Author: czr@cesar.org.br
 */

#include "util/uptime.h"
#include "stm32l0xx_hal.h"

#include <time.h>
#include <stdint.h>

int uptime_get_uptime(struct timespec *up)
{
	uint32_t ticks = HAL_GetTick();

	up->tv_sec = ticks / 1000;
	up->tv_nsec = (ticks % 1000) * 1000000;

	return 0;
}

int uptime_timecmp(const struct timespec *t1, const struct timespec *t2)
{
	long secdiff = t1->tv_sec - t2->tv_sec;
	long nsecdiff = t1->tv_nsec - t2->tv_nsec;

	if(secdiff > 0) {
		return 1;

	} else if (secdiff < 0) {
		return -1;

	} else {

		if(nsecdiff > 0) {
			return 1;
		} else if(nsecdiff < 0) {
			return -1;
		} else {
			return 0;
		}
	}

}
