/*
 * crc8.c
 *
 *  Created on: Jan 2, 2018
 *      Author: czr
 */

#include "util/crc8.h"

#include <stdint.h>

#define CRC8_POLYNOMIAL		0x07
#define CRC8_TABLE_SIZE		256
#define CRC8_CRC_INIT_VAL	0xff

static uint8_t crc8_table[CRC8_TABLE_SIZE];

void crc8_table_populate(void)
{
	int i, j;
	uint8_t t = 1;

	crc8_table[0] = 0;

	for (i = (256 >> 1); i; i >>= 1) {
		t = (t >> 1) ^ (t & 1 ? CRC8_POLYNOMIAL : 0);
		for (j = 0; j < CRC8_TABLE_SIZE; j += 2*i)
			crc8_table[i+j] = crc8_table[j] ^ t;
	}
}

uint8_t crc8_calc(const void *data, uint32_t data_len)
{
	uint8_t crc = CRC8_CRC_INIT_VAL;
	const uint8_t *pdata;
	pdata = (const uint8_t *)data;

	while (data_len-- > 0)
		crc = crc8_table[(crc ^ *pdata++) & 0xff];

	return crc;

}
