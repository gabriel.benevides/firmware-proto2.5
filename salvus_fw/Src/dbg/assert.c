/*
 * assert.c
 *
 *  Created on: Dec 1, 2017
 *      Author: czr@cesar.org.br
 */

#ifdef USE_FULL_ASSERT

#include <stdint.h>
#include "dbg/dbg.h"

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
	dbg_printf("assert_failed(): wrong parameter value at");
	dbg_printf("%s:%d\r\n", (char *)file, line);
}

#endif
