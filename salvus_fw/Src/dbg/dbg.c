/*
 * dbg.c
 *
 *  Created on: Dec 1, 2017
 *      Author: czr@cesar.org.br
 */

#include "dbg/dbg.h"
#include "hw/hw_init.h"

#include "stm32l0xx_hal.h"
#ifndef DEBUG_CDC
#include "stm32l0xx_hal_uart.h"
#else
#include "usb_device.h"
#include "usbd_cdc_if.h"
#endif

#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>

#ifdef USE_DEBUG
#define DBG_DEFAULT_STRING_SIZE		80
static char string[DBG_DEFAULT_STRING_SIZE];
#endif

#ifndef DEBUG_CDC
#define DBG_UART	&debug_uart
#endif

int dbg_printf(const char *fmt, ...)
{
#ifdef USE_DEBUG
	int size;
	va_list args;

	va_start(args, fmt);
	size = dbg_vprintf(fmt, args);
	va_end(args);

	return size;
#else
	(void)fmt;
	return 0;
#endif
}

int dbg_vprintf(const char *fmt, va_list args)
{
#ifdef USE_DEBUG
	int size;

	size = vsnprintf(string, DBG_DEFAULT_STRING_SIZE, fmt, args);
#ifndef DEBUG_CDC
	HAL_UART_Transmit(DBG_UART, (uint8_t *)string, size, HAL_MAX_DELAY);
#else
	CDC_Transmit_FS((uint8_t *)string, size);
#endif
	return size;
#else
	(void)fmt; (void)args;
	return 0;
#endif
}

int dbg_debug(const char *fmt, ...)
{
#ifdef USE_DEBUG
	int size;
	va_list args;

	size = dbg_printf("[DEBUG] ");
	va_start(args, fmt);
	size += dbg_vprintf(fmt, args);
	va_end(args);

	return size;
#else
	(void)fmt;
	return 0;
#endif
}

int dbg_info(const char *fmt, ...)
{
#ifdef USE_DEBUG
	int size;
	va_list args;

	size = dbg_printf("[INFO ] ", __FILE__, __LINE__);
	va_start(args, fmt);
	size += dbg_vprintf(fmt, args);
	va_end(args);

	return size;
#else
	(void)fmt;
	return 0;
#endif
}

int dbg_warn(const char *fmt, ...)
{
#ifdef USE_DEBUG
	int size;
	va_list args;

	size = dbg_printf("[WARN ] ", __FILE__, __LINE__);
	va_start(args, fmt);
	size += dbg_vprintf(fmt, args);
	va_end(args);

	return size;
#else
	(void)fmt;
	return 0;
#endif
}

int dbg_err(const char *fmt, ...)
{
#ifdef USE_DEBUG
	int size;
	va_list args;

	size = dbg_printf("[ERROR] ", __FILE__, __LINE__);
	va_start(args, fmt);
	size += dbg_vprintf(fmt, args);
	va_end(args);

	return size;
#else
	(void)fmt;
	return 0;
#endif
}

int dbg_log(const char *fmt, ...)
{
#ifdef USE_DEBUG
	int size;
	va_list args;

	size = dbg_printf("[ LOG ] ");
	va_start(args, fmt);
	size += dbg_vprintf(fmt, args);
	va_end(args);

	return size;
#else
	(void)fmt;
	return 0;
#endif
}
