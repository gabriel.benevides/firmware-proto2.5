/*
 * hw_autotest.c
 *
 *  Created on: Mar 20, 2018
 *      Author: czr@cesar.org.br
 */

#include "hw/hw_autotest.h"
#include "hw/adc.h"
#include "hw/tamper.h"
#include "hw/leds.h"
#include "sigfox/sigfox.h"

#include "stm32l0xx_hal.h"

/* TODO: Check values and set correct thresholds */

#ifdef BOARD_SIGFOX
/*
 * d = 3,0/4096 = 0,000732422 V/LSB
 * 0,5 V_FULLRANGE => 0,334V
 * 0,334 / d = 414 LSB
 */
#define HW_AUTOTEST_ADC_PRESSURE_MIN_VAL		200//456
#define HW_AUTOTEST_ADC_DIFF_PRESSURE_MIN_VAL	200//456	// for now

#else
#error "Please specify a board: BOARD_SIGFOX"
#endif

hw_autotest_e hw_run_autotest(void) {

	hw_autotest_e ret = AUTOTEST_SUCCESS;
	adc_calibration_t calib;

	/* Checks the communication */
#ifdef BOARD_SIGFOX
	sigfox_status_e st_sigfox = sigfox_is_alive();
	if (st_sigfox != SIGFOX_OK) {
		ret = AUTOTEST_COMM_FAIL;
		goto exit;
	}
#endif
	/* Checks ADC sensors */
	adc_pressure_get_calibration(&calib);
	if(calib.adc_pressure < HW_AUTOTEST_ADC_PRESSURE_MIN_VAL ||
			calib.adc_diff_pressure < HW_AUTOTEST_ADC_DIFF_PRESSURE_MIN_VAL) {

		ret = AUTOTEST_SENSORS_FAIL;
		goto exit;
	}

	/* Check anti-tamper detection */
	if(tamper_get_status() != TAMPER_OK) {
		ret = AUTOTEST_TAMPER_FAIL;
		goto exit;
	}

	exit:
	return ret;
}

void hw_autotest_fail(hw_autotest_e st) {

	/* TODO: Program behavior of autotest failure
	 * Maybe all LEDs blink? Maybe LEDs blink differently to mean failure
	 */

	while(1) {
		int blink_count = 0;

		switch(st) {
		case AUTOTEST_COMM_FAIL:
			blink_count = 5;
			break;

		case AUTOTEST_SENSORS_FAIL:
			blink_count = 4;
			break;

		case AUTOTEST_TAMPER_FAIL:
			blink_count = 3;
			break;

		default:
			break;
		}

		leds_alive_set(LEDS_ON);
		for(int i = 0; i < blink_count; i++) {
			leds_low_pressure_set(LEDS_ON);
			HAL_Delay(150);
			leds_low_pressure_set(LEDS_OFF);
			HAL_Delay(150);
		}
		HAL_Delay(1000);

		if(st == AUTOTEST_TAMPER_FAIL)
			break;
	}

}

const char *hw_autotest_str(hw_autotest_e st)
{
	switch(st){
	case AUTOTEST_SUCCESS:
		return "SUCCESS";

	case AUTOTEST_TAMPER_FAIL:
		return "TAMPER FAIL";

	case AUTOTEST_COMM_FAIL:
		return "COMMUNICATION FAIL";

	case AUTOTEST_SENSORS_FAIL:
		return "SENSOR FAIL";

	default:
		break;
	}

	return "UNKNOWN";
}
