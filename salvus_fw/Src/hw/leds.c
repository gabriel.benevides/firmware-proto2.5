/*
 * leds.c
 *
 *  Created on: Feb 20, 2018
 *      Author: czr@cesar.org.br
 */

#include "hw/leds.h"
#include "hw/hw_pin_defs.h"

#include "stm32l0xx_hal.h"
#include "stm32l0xx_hal_gpio.h"

void leds_alive_set(leds_state_e val)
{
	if(val == LEDS_ON)
		HAL_GPIO_WritePin(LEDS_HEARTBEAT_Port, LEDS_HEARTBEAT_Pin,
			GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(LEDS_HEARTBEAT_Port, LEDS_HEARTBEAT_Pin,
			GPIO_PIN_SET);
}

void leds_low_pressure_set(leds_state_e val)
{
	if(val == LEDS_ON)
		HAL_GPIO_WritePin(LEDS_LOW_PRESSURE_Port, LEDS_LOW_PRESSURE_Pin,
			GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(LEDS_LOW_PRESSURE_Port, LEDS_LOW_PRESSURE_Pin,
			GPIO_PIN_SET);
}

void leds_low_batt_set(leds_state_e val) {
	if(val == LEDS_ON)
		HAL_GPIO_WritePin(LEDS_LOW_BAT_Port, LEDS_LOW_BAT_Pin, GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(LEDS_LOW_BAT_Port, LEDS_LOW_BAT_Pin, GPIO_PIN_SET);
}

void leds_quick_blink(leds_e leds)
{
	if (leds & LEDS_ALIVE)			leds_alive_set(LEDS_ON);
	if (leds & LEDS_LOW_BATT)		leds_low_batt_set(LEDS_ON);
	if (leds & LEDS_LOW_PRESSURE)	leds_low_pressure_set(LEDS_ON);
	HAL_Delay(100);
	leds_alive_set(LEDS_OFF);
	leds_low_batt_set(LEDS_OFF);
	leds_low_pressure_set(LEDS_OFF);
}
