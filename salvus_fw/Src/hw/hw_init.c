/*
 * hw_init.c
 *
 *  Created on: Dec 1, 2017
 *      Author: czr@cesar.org.br
 */
#include <stdint.h>

#include "stm32l0xx_hal.h"
#include "hw/hw_pin_defs.h"

#include "stm32l0xx_hal_rtc.h"
#include "stm32l0xx_hal_rtc_ex.h"
#include "stm32l0xx_hal_adc.h"
#include "stm32l0xx_hal_dma.h"
#include "stm32l0xx_hal_pcd.h"

/* Debug - Default enabled USART: USART1 */
UART_HandleTypeDef debug_uart;

/* SIGFOX: LPUART1 */
UART_HandleTypeDef sigfox_uart;

/* GPRS: USART2 */
/*
UART_HandleTypeDef gprs_uart;
*/
/* RTC handler */
RTC_HandleTypeDef hrtc;

/* ADC + DMA handlers */
ADC_HandleTypeDef hadc;
DMA_HandleTypeDef hdma_adc;

/**
 * System Clock Configuration
 */
void SystemClock_Config(void)
{

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;

	/**Configure the main internal regulator output voltage
	 */
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI
			|RCC_OSCILLATORTYPE_LSE|RCC_OSCILLATORTYPE_HSI48;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLMUL = RCC_PLLMUL_4;
	RCC_OscInitStruct.PLL.PLLDIV = RCC_PLLDIV_2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
		if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
		{
			_Error_Handler(__FILE__, __LINE__);
		}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1
			|RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_LPUART1|RCC_PERIPHCLK_RTC
			|RCC_PERIPHCLK_USB;
	PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
	PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
	PeriphClkInit.Lpuart1ClockSelection = RCC_LPUART1CLKSOURCE_HSI;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
	PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USART1 init function */
void MX_USART1_UART_Init(void)
{
	debug_uart.Instance = USART1;
	debug_uart.Init.BaudRate = 115200;
	debug_uart.Init.WordLength = UART_WORDLENGTH_8B;
	debug_uart.Init.StopBits = UART_STOPBITS_1;
	debug_uart.Init.Parity = UART_PARITY_NONE;
	debug_uart.Init.Mode = UART_MODE_TX_RX;
	debug_uart.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	debug_uart.Init.OverSampling = UART_OVERSAMPLING_16;
	debug_uart.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	debug_uart.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&debug_uart) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}
}


/* USART2 init function - GPRS */

/*
void MX_USART2_UART_Init(void)
{
	gprs_uart.Instance = USART2;
	gprs_uart.Init.BaudRate = 9600;
	gprs_uart.Init.WordLength = UART_WORDLENGTH_8B;
	gprs_uart.Init.StopBits = UART_STOPBITS_1;
	gprs_uart.Init.Parity = UART_PARITY_NONE;
	gprs_uart.Init.Mode = UART_MODE_TX_RX;
	gprs_uart.Init.HwFlowCtl = UART_HWCONTROL_RTS_CTS;
	gprs_uart.Init.OverSampling = UART_OVERSAMPLING_16;
	gprs_uart.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	gprs_uart.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&gprs_uart) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}
}
*/

/* LPUART1 init function */
void MX_LPUART1_UART_Init(void)
{
	sigfox_uart.Instance = LPUART1;
	sigfox_uart.Init.BaudRate = 9600;
	sigfox_uart.Init.WordLength = UART_WORDLENGTH_8B;
	sigfox_uart.Init.StopBits = UART_STOPBITS_1;
	sigfox_uart.Init.Parity = UART_PARITY_NONE;
	sigfox_uart.Init.Mode = UART_MODE_TX_RX;
	sigfox_uart.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	sigfox_uart.Init.OverSampling = UART_OVERSAMPLING_16;
	sigfox_uart.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	sigfox_uart.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	if (HAL_UART_Init(&sigfox_uart) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}
}

/** Configure pins as
 * Analog
 * Input
 * Output
 * EVENT_OUT
 * EXTI
 * Free pins are configured automatically as Analog (this feature is
 * enabled through
 * the Code Generation settings)
 */
void MX_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();

	/*Configure GPIO pins : PC0 PC1 PC2 PC3 PC6 PC7 PC8 PC9 PC10 PC11 PC12 */
	GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/*Configure GPIO pins : PA0 PA1 PA2 PA3 PA6 PA7 PA8 PA11 PA12 PA15 */
	GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_6|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_15;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	/*Configure GPIO pins : PB2 PB12 PB13 PB14 PB15 PB6 PB7 */            
	GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15|GPIO_PIN_6|GPIO_PIN_7;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

	/////////////////////////////////////////////////////////////////////
	/* Configuration of used PINs as non-analog input */

	/* LEDs pins */
	GPIO_InitStruct.Pin = LEDS_HEARTBEAT_Pin|LEDS_LOW_BAT_Pin
			|LEDS_LOW_PRESSURE_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(LEDS_HEARTBEAT_Port, &GPIO_InitStruct);

	/* User-switch */
	GPIO_InitStruct.Pin = MISC_USER_BUTTON_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(MISC_USER_BUTTON_Port, &GPIO_InitStruct);
	
	/* SIGFOX */
	GPIO_InitStruct.Pin = SIGFOX_Reset_Pin|SIGFOX_Wakeup_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(SIGFOX_Reset_Port, &GPIO_InitStruct);
	
	/*P5V0*/
	GPIO_InitStruct.Pin = MISC_P5V0_EN_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(MISC_P5V0_EN_Port, &GPIO_InitStruct);
	
	/*P3V0_RF*/
	GPIO_InitStruct.Pin = MISC_P3V0_RF_EN_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(MISC_P3V0_RF_EN_Port, &GPIO_InitStruct);
	
	//SARA DESCONTINUADO NO PROTO 2.5
	/* SARA-G350 */
	/*
	GPIO_InitStruct.Pin = SARA_PWR_ON_Pin|SARA_RESET_N_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(SARA_PWR_ON_Port, &GPIO_InitStruct);
	*/
	//SINAL DESCONTINUADO NO PROTO 2.5
	/* Powerpath Status 1 */
	/*
	GPIO_InitStruct.Pin = POWER_MCU_PP_STAT1_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(POWER_MCU_PP_STAT1_Port, &GPIO_InitStruct);
	*/
}

void MX_RTC_Init(void)
{
	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;

	/**Initialize RTC Only */
	hrtc.Instance = RTC;
	hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
	hrtc.Init.AsynchPrediv = 127;
	hrtc.Init.SynchPrediv = 255;
	hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
	hrtc.Init.OutPutRemap = RTC_OUTPUT_REMAP_NONE;
	hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
	hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
	if (HAL_RTC_Init(&hrtc) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Initialize RTC and set the Time and Date */
	if(HAL_RTCEx_BKUPRead(&hrtc, RTC_BKP_DR0) != 0x32F2){
		sTime.Hours = 0x0;
		sTime.Minutes = 0x0;
		sTime.Seconds = 0x0;
		sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
		sTime.StoreOperation = RTC_STOREOPERATION_RESET;
		if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
		{
			_Error_Handler(__FILE__, __LINE__);
		}

		sDate.WeekDay = RTC_WEEKDAY_MONDAY;
		sDate.Month = RTC_MONTH_JANUARY;
		sDate.Date = 0x1;
		sDate.Year = 0x0;

		if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
		{
			_Error_Handler(__FILE__, __LINE__);
		}

		HAL_RTCEx_BKUPWrite(&hrtc,RTC_BKP_DR0,0x32F2);
	}

	if (HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, 30*2048, RTC_WAKEUPCLOCK_RTCCLK_DIV16) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}
}

void MX_ADC_Init(void)
{
	ADC_ChannelConfTypeDef sConfig;
	GPIO_InitTypeDef GPIO_InitStruct;

	/* Configure the global features of the ADC (Clock, Resolution,
	 * Data Alignment and number of conversion) */
	hadc.Instance = ADC1;
	hadc.Init.OversamplingMode = DISABLE;
	hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
	hadc.Init.Resolution = ADC_RESOLUTION_12B;
	hadc.Init.SamplingTime = ADC_SAMPLETIME_160CYCLES_5;
	hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
	hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc.Init.ContinuousConvMode = ENABLE;
	hadc.Init.DiscontinuousConvMode = DISABLE;
	hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	hadc.Init.DMAContinuousRequests = ENABLE;
	hadc.Init.EOCSelection = ADC_EOC_SEQ_CONV;
	hadc.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
	hadc.Init.LowPowerAutoWait = DISABLE;
	hadc.Init.LowPowerFrequencyMode = DISABLE;
	hadc.Init.LowPowerAutoPowerOff = DISABLE;
	if (HAL_ADC_Init(&hadc) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/* Configure for the selected ADC regular channel to be converted */
	sConfig.Channel = ADC_CHANNEL_4;
	sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
	if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	sConfig.Channel = ADC_CHANNEL_5;
	if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/* ADC for the USER inputs (PVBATT and 5V) */
	sConfig.Channel = ADC_CHANNEL_8;
	if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	sConfig.Channel = ADC_CHANNEL_9;
	if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	__HAL_RCC_GPIOA_CLK_ENABLE();
	GPIO_InitStruct.Pin = ADC_PRESSURE_VOUT_Pin|ADC_FLOW_VOUT_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(ADC_PRESSURE_VOUT_Port, &GPIO_InitStruct);

	/* User ADCs */
	__HAL_RCC_GPIOB_CLK_ENABLE();
	GPIO_InitStruct.Pin = MISC_MCU_ADC_IN8_Pin|MISC_MCU_ADC_IN9_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(MISC_MCU_ADC_IN8_Port, &GPIO_InitStruct);

	/* Configures DMA to read channel 1 */
	hdma_adc.Instance = DMA1_Channel1;
	hdma_adc.Init.Request = DMA_REQUEST_0;
	hdma_adc.Init.Direction = DMA_PERIPH_TO_MEMORY;
	hdma_adc.Init.PeriphInc = DMA_PINC_DISABLE;
	hdma_adc.Init.MemInc = DMA_MINC_ENABLE;
	hdma_adc.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
	hdma_adc.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
	hdma_adc.Init.Mode = DMA_CIRCULAR;
	hdma_adc.Init.Priority = DMA_PRIORITY_MEDIUM;
	if (HAL_DMA_Init(&hdma_adc) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	__HAL_LINKDMA(&hadc, DMA_Handle, hdma_adc);
}

void MX_DMA_Init(void)
{
	/* DMA controller clock enable */
	__HAL_RCC_DMA1_CLK_ENABLE();

	/* DMA interrupt init */
	/* DMA1_Channel1_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 1, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */
void _Error_Handler(char * file, int line)
{
	(void)file; (void)line;
	while(1)
	{
	}
}
