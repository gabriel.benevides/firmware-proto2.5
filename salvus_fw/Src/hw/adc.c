/*
 * adc.c
 *
 *  Created on: Dec 21, 2017
 *      Author: czr@cesar.org.br
 */

#include "hw/adc.h"
#include "hw/hw_init.h"
#include "stm32l0xx_hal.h"
#include "stm32l0xx_hal_adc.h"
#include "stm32l0xx_hal_dma.h"
#include <math.h>

#ifdef BOARD_SIGFOX
#define ADC_MAX_VALUE_F	4095.0f
#define ADC_DELTA		0.000732422f
#define ADC_MIN_VALUE_F 682.666550158f
#elif defined BOARD_GPRS
#warning "GPRS board is not done yet"
#define ADC_MAX_VALUE_F	4095.0f
#define ADC_DELTA		0.000854492f;
#define ADC_MIN_VALUE_F	585.142985539f
#else
#error "Please specify board: BOARD_SIGFOX or BOARD_GPRS"
#endif

#define ADC_PRESSURE_START_IDX		0
#define ADC_DIFF_PRESSURE_START_IDX	1
#define ADC_USER_ADC_IDX			2
#define ADC_PVBATT_START_IDX		3

#define PRESSURE_3000PSI_MBAR	206842.7f

static float adc_pressure_zero_val;
static float mbar_per_lsb = 0.0f;

#define SAMPLE_SIZE 		64
#define NUMBER_OF_ADCS		4
static uint32_t pressure_adc[SAMPLE_SIZE*NUMBER_OF_ADCS]; // Two DMA channels
static float adc_diff_pressure_zero_val;

/**
 * DIFF_PRESSURE_MODEL_THRESHOLD
 * Specifies the threshold for different models that try to minimise error
 * for the flow measurement. This value separates the models for flows below
 * 4l/min and +4l/min
 */
#define DIFF_PRESSURE_MODEL_THRESHOLD	131.0f

void adc_start_conversion(void)
{
	/* Runs the calibration once! */
	static int calibration_done = 0;
	if(calibration_done == 0) {
		HAL_ADCEx_Calibration_Start(&hadc, ADC_SINGLE_ENDED);
		calibration_done = 1;
	}

	/* Enables DMA acess to SAMPLE_SIZE samples of the ADC */
	HAL_ADC_Start_DMA(&hadc, pressure_adc, SAMPLE_SIZE*NUMBER_OF_ADCS);
}

uint32_t adc_pressure_read_pressure(void)
{
	uint32_t ret = 0;
	float tmp;

	for(int i = ADC_PRESSURE_START_IDX; i < SAMPLE_SIZE*NUMBER_OF_ADCS;
		 i+=NUMBER_OF_ADCS) {
		ret += pressure_adc[i];
	}

	ret = ret / SAMPLE_SIZE;

	/* Converting the ADC value to a 8.4 fix-point value */
	tmp = (float)ret;
	tmp = (tmp - adc_pressure_zero_val) * mbar_per_lsb;
	/* Converts from mbar to bar */
	tmp /= 1000.0f;

	if(tmp < 0.0f) tmp = 0.0f;
	ret = (uint16_t)(tmp * 16.0f);

	return ret;
}

uint32_t adc_diff_pressure_read_pressure(void)
{
	uint32_t ret = 0;
	float tmp;

	for(int i = ADC_DIFF_PRESSURE_START_IDX; i < SAMPLE_SIZE*NUMBER_OF_ADCS;
		i+=NUMBER_OF_ADCS) {
		ret += pressure_adc[i];
	}

	ret = ret / SAMPLE_SIZE;

	tmp = (float)ret - adc_diff_pressure_zero_val;
	if(tmp < 0.0f)	tmp = 0.0f;

	// Chooses model to calculate the O2 flow depending on measured diff pressure
	if(tmp < DIFF_PRESSURE_MODEL_THRESHOLD) {
		// Model 4: a*sqrtf(x + b) + c
		tmp = 0.318208*sqrtf(tmp + 3.105785) - 0.567792;
		if(tmp < 0.0f)	tmp = 0.0f;
	} else {
		// Model 2: a*sqrtf(x) + b
		tmp = 0.309293*sqrtf(tmp) - 0.322520;
		if(tmp < 0.0f)	tmp = 0.0f;
	}

	/* Converts to 8.4 fixed-point */
	tmp = tmp * 16.0f;
	ret = (uint32_t)tmp;

	return ret;
}

void adc_stop_conversion(void)
{
	/* Disables the DMA sampling */
	HAL_ADC_Stop_DMA(&hadc);
}

static uint32_t adc_pressure_read_value(void)
{
	uint32_t ret = 0;

	for(int i = ADC_PRESSURE_START_IDX; i < SAMPLE_SIZE*NUMBER_OF_ADCS;
		i+=NUMBER_OF_ADCS) {
		ret += pressure_adc[i];
	}

	ret = ret / SAMPLE_SIZE;

	return ret;
}

static uint32_t adc_diff_pressure_read_value(void)
{
	uint32_t ret = 0;

	for(int i = ADC_DIFF_PRESSURE_START_IDX; i < SAMPLE_SIZE*NUMBER_OF_ADCS;
		i+=NUMBER_OF_ADCS) {
		ret += pressure_adc[i];
	}
	ret = ret / SAMPLE_SIZE;

	return ret;
}


void adc_pressure_calibrate(adc_calibration_t *calib)
{
	uint32_t sum_diff_pressure = 0;
	uint32_t sum_pressure = 0;

	for(int i = 0; i < 50; i++) {
		sum_pressure += adc_pressure_read_value();
		sum_diff_pressure += adc_diff_pressure_read_value();
		// HAL_Delay(100);
	}

	adc_pressure_zero_val = ADC_MIN_VALUE_F;
	adc_diff_pressure_zero_val = (float)sum_diff_pressure / 50.0f;

	/*
	 * mbar_per_lsb
	 * 3000 psi = 206842,7 mbar
	 * Pressure ADC voltage base level = pressure_adc_zero_val LSB
	 * mbar_per_lsb = 206842,7 mbar / (ADC_MAX_VALUE_F - ADC_MIN_VALUE_F)
	 */
	/* Values for 2000 psi sensor */
	// mbar_per_lsb = 137895.1f/(4095.0f - ADC_MIN_VALUE_F);

	/* Values for 3000 psi sensor */
	mbar_per_lsb = PRESSURE_3000PSI_MBAR /
		(ADC_MAX_VALUE_F - ADC_MIN_VALUE_F);

	calib->adc_pressure = sum_pressure / 50;
	calib->adc_diff_pressure = sum_diff_pressure / 50;
}

void adc_pressure_get_calibration(adc_calibration_t *calib)
{
	calib->adc_pressure = (uint32_t)adc_pressure_zero_val;
	calib->adc_diff_pressure = (uint32_t)adc_diff_pressure_zero_val;
}

uint32_t adc_read_pvbat_value(void)
{
	uint32_t ret = 0;

	for(int i = ADC_PVBATT_START_IDX; i < SAMPLE_SIZE*NUMBER_OF_ADCS;
		i+=NUMBER_OF_ADCS) {
		ret += pressure_adc[i];
	}
	ret /= SAMPLE_SIZE;

	return ret;
}

uint32_t adc_read_pvbat_voltage(void)
{
	uint32_t ret = adc_read_pvbat_value();

	/* ADC_DELTA * 2 is necessary because of voltage divisor */
	float voltage = ADC_DELTA * 2.0f * (float)ret;
	voltage *= 16.0f;
	ret = (uint32_t)voltage;

	return ret;
}
