/*
 * sensors.c
 *
 *  Created on: Jan 4, 2018
 *      Author: czr@cesar.org.br
 */
#include "hw/sensors.h"
#include "hw/adc.h"

void sensors_read(sensor_type_e sensor_idx, sensor_t *sensor)
{
	switch(sensor_idx) {
	case SENSOR_TYPE_PRESSURE:
		sensor->sensor.sensorType	= SENSOR_TYPE_PRESSURE;
		sensor->sensor.sensorValue	= (uint16_t)
			(adc_pressure_read_pressure() & 0x0fff);
		break;

	case SENSOR_TYPE_DIFF_PRESSURE:
		sensor->sensor.sensorType = SENSOR_TYPE_DIFF_PRESSURE;
		sensor->sensor.sensorValue = (uint16_t)
			(adc_diff_pressure_read_pressure() & 0xfff);
		break;

	case SENSOR_BATT_MEASUREMENT:
	sensor->sensor.sensorType = SENSOR_BATT_MEASUREMENT;
	sensor->sensor.sensorValue = (uint16_t)
		(adc_read_pvbat_voltage() & 0xfff);
		break;

	default:
		sensor->sensor.sensorType	= 0x0F;
		sensor->sensor.sensorValue	= 0x0000;
		break;
	}
}
