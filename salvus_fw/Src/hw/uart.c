/*
 * uart.c
 *
 *  Created on: Dec 1, 2017
 *      Author: czr@cesar.org.br
 */

#include "hw/uart.h"
#include "hw/hw_init.h"

#include "stm32l0xx_hal.h"
#include "stm32l0xx_hal_uart.h"

#include <stdio.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>

#define DEFAULT_STRING_SIZE		80

/* TODO: make the transmission via ISR */
ssize_t uart_write(UART_HandleTypeDef *huart, const void *buf, size_t num)
{
	(void)huart;
	HAL_UART_Transmit(huart, (uint8_t *)buf, num, HAL_MAX_DELAY);
	return (ssize_t)num;
}

/* TODO: make the reception via ISR */
ssize_t uart_read(UART_HandleTypeDef *huart, void *buf, size_t num)
{
	(void)huart;
	HAL_UART_Receive(huart, (uint8_t *)buf, num, HAL_MAX_DELAY);

	return (ssize_t)num;
}

ssize_t uart_read_string(UART_HandleTypeDef *huart, char *str, size_t len)
{
	ssize_t idx, slen;

	memset(str, 0x00, len);
	idx = 0;
	do {
		HAL_UART_Receive(huart, (uint8_t *)&str[idx], len - idx, 10);
		slen = strlen((char *)&str[idx]);
		idx += slen;
	} while (slen != 0);

	return idx;
}

int uart_printf(UART_HandleTypeDef *huart, const char *fmt, ...)
{
	int size;
	va_list args;

	va_start(args, fmt);
	size = uart_vprintf(huart, fmt, args);
	va_end(args);

	return size;
}

int uart_vprintf(UART_HandleTypeDef *huart, const char *fmt, va_list args)
{
	int size;
	char string[DEFAULT_STRING_SIZE];

	size = vsnprintf(string, DEFAULT_STRING_SIZE, fmt, args);
	uart_write(huart, (uint8_t *)string, size);

	return size;
}
