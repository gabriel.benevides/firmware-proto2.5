/*
 * batt.c
 *
 *  Created on: Mar 21, 2018
 *      Author: czr@cesar.org.br
 */

#include "hw/batt.h"
#include "hw/adc.h"

 /*
  * AA Alkaline Battery has a very steep voltage drop after 1.0V.
  * 1.25V -> Low-battery: message is sent to backend
  * 1.1V -> Very-low battery: led starts blinking
  *
  * 1.25V x 3 batteries = 3.75V -> After voltage divisor = 1.875V
  * 1.1V x 3 batteries = 3.3V -> After voltage divisor = 1.65V
  * 1.875V -> 1.875/3.0 * 4096 = 2560
  * 1.65V -> 1.65/3.0 * 4096 =
  */

#ifdef BOARD_SIGFOX
#define PVBAT_MSG_LOW_THRESHOLD	2560
#define PVBAT_LED_LOW_THRESHOLD	2253
#elif defined BOARD_GPRS
#define PVBAT_MSG_LOW_THRESHOLD	2194
#define PVBAT_LED_LOW_THRESHOLD	1931
#else
#error "Please specify board: BOARD_SIGFOX or BOARD_GPRS"
#endif

batt_voltage_e batt_check_voltage(void)
{
	uint32_t pvbat = adc_read_pvbat_value();
	if(pvbat <= PVBAT_MSG_LOW_THRESHOLD && pvbat > PVBAT_LED_LOW_THRESHOLD) {
		return 	BATT_MSG_LOW_VOLTAGE;

	} else if (pvbat <= PVBAT_LED_LOW_THRESHOLD) {
		return BATT_LED_LOW_VOLTAGE;
	}

	return BATT_NORMAL_VOLTAGE;
}

uint32_t batt_measure_voltage(void)
{
	return adc_read_pvbat_value();
}
