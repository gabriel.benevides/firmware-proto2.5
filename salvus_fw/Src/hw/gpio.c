/*
 * input.c
 *
 *  Created on: Apr 26, 2018
 *      Author: czr@cesar.org.br
 */

#include "hw/gpio.h"
#include "hw/hw_pin_defs.h"

#include <stm32l0xx_hal.h>
#include <stm32l0xx_hal_gpio.h>

gpio_e input_read_user_button(void)
{
    return HAL_GPIO_ReadPin(MISC_USER_BUTTON_Port, MISC_USER_BUTTON_Pin);
}

gpio_e input_read_power_good(void)
{
    return HAL_GPIO_ReadPin(POWER_P3V_GOOD_Port, POWER_P3V_GOOD_Pin);
}

powerpath_e input_powerpath_status(void)
{
    GPIO_PinState st;

    st = HAL_GPIO_ReadPin(POWER_MCU_PP_STAT1_Port, POWER_MCU_PP_STAT1_Pin);
    if(st == GPIO_PIN_SET)  return POWERPATH_VBAT;
    else                    return POWERPATH_USB;
}

void output_sigfox_reset(gpio_e st)
{
    if(st == HIGH) {
        HAL_GPIO_WritePin(SIGFOX_Reset_Port, SIGFOX_Reset_Pin, GPIO_PIN_SET);
    } else {
        HAL_GPIO_WritePin(SIGFOX_Reset_Port, SIGFOX_Reset_Pin, GPIO_PIN_RESET);
    }
}

void output_sigfox_wakeup(gpio_e st)
{
    if(st == HIGH) {
        HAL_GPIO_WritePin(SIGFOX_Wakeup_Port, SIGFOX_Wakeup_Pin, GPIO_PIN_SET);
    } else {
        HAL_GPIO_WritePin(SIGFOX_Wakeup_Port, SIGFOX_Wakeup_Pin, GPIO_PIN_RESET);
    }
}

void output_sara_reset_n(gpio_e st)
{
    if(st == HIGH) HAL_GPIO_WritePin(SARA_RESET_N_Port, SARA_RESET_N_Pin, GPIO_PIN_SET);
    else HAL_GPIO_WritePin(SARA_RESET_N_Port, SARA_RESET_N_Pin, GPIO_PIN_RESET);
    
}

void output_sara_pwr_on(gpio_e st)
{
    if(st == HIGH) HAL_GPIO_WritePin(SARA_PWR_ON_Port, SARA_PWR_ON_Pin, GPIO_PIN_SET);
    else HAL_GPIO_WritePin(SARA_PWR_ON_Port, SARA_PWR_ON_Pin, GPIO_PIN_RESET);
}


void P3V0_RF_EN (gpio_e st)
{
	if(st == HIGH)
	{ 
		HAL_GPIO_WritePin(MISC_P3V0_RF_EN_Port, MISC_P3V0_RF_EN_Pin, GPIO_PIN_SET);
    	}
    	else 
    	{
    		HAL_GPIO_WritePin(MISC_P3V0_RF_EN_Port, MISC_P3V0_RF_EN_Pin, GPIO_PIN_RESET);	
	}
}

void P5V0_EN (gpio_e st)
{
	if(st == HIGH)
	{
	 HAL_GPIO_WritePin(MISC_P5V0_EN_Port, MISC_P5V0_EN_Pin, GPIO_PIN_SET);
    	}
    	else
    	{ 
    	HAL_GPIO_WritePin(MISC_P5V0_EN_Port, MISC_P5V0_EN_Pin, GPIO_PIN_RESET);	
	}
}


