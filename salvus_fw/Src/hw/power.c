/*
 * power.c
 *
 *  Created on: Feb 16, 2018
 *      Author: czr@cesar.org.br
 */

#include "hw/power.h"
#include "hw/hw_init.h"
#include "hw/hw_autotest.h"
#include "hw/uart.h"
#include "hw/gpio.h"
#include "dbg/dbg.h"
#include "sigfox/sigfox.h"

#include "stm32l0xx_hal.h"
#include "stm32l0xx_hal_pwr.h"
#include "stm32l0xx_hal_rtc.h"
#include "stm32l0xx_hal_rtc_ex.h"

static usb_status_e usb_is_connected = 0;
sigfox_status_e st;

void power_switch_to_sleep_mode(void)
{
	dbg_debug("Entrando no sleep mode\r\n");
#ifdef BOARD_SIGFOX
	// uart_printf(&sigfox_uart, "AT$P=2\r\n");
	output_sigfox_reset(LOW);
	P3V0_RF_EN (LOW);
#elif defined BOARD_GPRS
#endif

	/*Disabling sensors power*/
	P5V0_EN(LOW);
	/* Must disable all interrupts that can happen for correct sleep */
	HAL_SuspendTick();
	HAL_NVIC_DisableIRQ(DMA1_Channel1_IRQn);

	HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
}

void power_switch_to_run_mode(void)
{
	dbg_debug("Entrando no run mode\r\n");
	HAL_ResumeTick();
	HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

#ifdef BOARD_SIGFOX
	/* Must toggle WAKEUP pin to wake the SIGFOX module */
	// output_sigfox_wakeup(LOW);
	// output_sigfox_wakeup(HIGH);
	/*Enabling sigfox power*/
	P3V0_RF_EN (HIGH);
	output_sigfox_reset(HIGH);
#elif defined BOARD_GPRS
	/* Wakeup SARA-G350 here? */
#endif
	/*Enabling sensors power*/
	P5V0_EN(HIGH);
	HAL_Delay(1000);
}

void power_usb_set_connected(usb_status_e st)
{
	usb_is_connected = st;
}

usb_status_e power_usb_is_connected(void)
{
	return usb_is_connected;
}
