/*
 * sigfox.c
 *
 *  Created on: Dec 1, 2017
 *      Author: czr@cesar.org.br
 */

#include "sigfox/sigfox.h"
#include "hw/uart.h"
#include "dbg/dbg.h"

#include <string.h>

static uint8_t ans[80];

#define SIGFOX_MAX_PAYLOAD 	12

static void sigfox_cleanup(void)
{
	int s;
	do {
		uart_printf(&sigfox_uart, "\r\n");
		s = uart_read_string(&sigfox_uart, (char *)ans, sizeof(ans));
	} while (s > 0);
}

const char *sigfox_status_str(sigfox_status_e st)
{
	switch(st) {
	case SIGFOX_OK:
		return "SIGFOX_OK";
	case SIGFOX_ERR:
		return "SIGFOX_ERR";
	case SIGFOX_INVALID_PAYLOAD:
		return "SIGFOX_INVALID_PAYLOAD";
	case SIGFOX_ERR_UNKNOWN_PROCESSOR:
		return "SIGFOX_ERR_UNKNOWN_PROCESSOR";

	default:
		return "UNKNOWN";
	}
}

sigfox_status_e sigfox_print_info(void)
{

	sigfox_status_e ret = SIGFOX_OK;

	sigfox_cleanup();

	for(int i = 0; i < 12; i++) {
		uart_printf(&sigfox_uart, "AT$I=%d\r\n", i);
		while(HAL_UART_GetState(&sigfox_uart) == HAL_UART_STATE_BUSY_TX);
		uart_read_string(&sigfox_uart, (char *)ans, sizeof(ans));

		dbg_info("AT$I=%d", i);
		dbg_printf(": ans=%s", ans);
	}

	return ret;
}

sigfox_status_e sigfox_init(void)
{
	sigfox_status_e ret = SIGFOX_OK;
	uart_printf(&sigfox_uart, "AT$I=0\r\n");
	while(HAL_UART_GetState(&sigfox_uart) == HAL_UART_STATE_BUSY_TX);
	uart_read_string(&sigfox_uart, (char *)ans, sizeof(ans));
	dbg_debug("Sigfox Init: %s\r\n", ans);
	if(strstr((char *)ans, "AX") == NULL)
	{
		ret = SIGFOX_ERR_UNKNOWN_PROCESSOR;
	}

	/* TODO: maybe more configurations here?? */

	return ret;
}

sigfox_status_e sigfox_is_alive(void)
{
	sigfox_status_e ret = SIGFOX_OK;

	sigfox_cleanup();

	uart_printf(&sigfox_uart, "\r\nAT\r\n");
	while(HAL_UART_GetState(&sigfox_uart) == HAL_UART_STATE_BUSY_TX);
	uart_read_string(&sigfox_uart, (char *)ans, sizeof(ans));

	if(strncmp("OK", (char *)ans, 2) != 0)
		ret = SIGFOX_ERR;

	return ret;
}

sigfox_status_e sigfox_send_frame(sigfox_msg_t *msg)
{
	sigfox_status_e ret = SIGFOX_OK;
	size_t count;

	if(msg->tx_msg_size > SIGFOX_MAX_PAYLOAD) {
		ret = SIGFOX_INVALID_PAYLOAD;
		goto exit;
	}

	/* Sends AT$RC to prepare to send packet. The user manual says that this
	 * is necessary only if AT$GI? returns X == || Y < 3. Since this call has
	 * no (apparent) side-effect it's being called independent of the state of
	 * AT$GI?
	 */
	uart_printf(&sigfox_uart, "AT$RC\r\n");
	uart_read_string(&sigfox_uart, (char *)ans, 80);
	if(strncmp((char *)ans, "OK", 2) != 0) {
		dbg_err("ans=%s\r\n", ans);
		ret = SIGFOX_ERR;
		goto exit;
	}

	uart_printf(&sigfox_uart, "AT$SF=");
	for(size_t i = 0; i < msg->tx_msg_size; i++) {
		uart_printf(&sigfox_uart, "%.2X", msg->tx_msg[i]);
	}
	uart_printf(&sigfox_uart, ",%d\r\n", msg->request_downlink);

	/* Must now wait for the return from the module */
	if(msg->request_downlink == DOWNLINK_FALSE) {
		/* If no downlink is requested then the module must return OK */
		count = 0;
		int tries = 0;
		do {
			count += uart_read_string(&sigfox_uart, (char *)&ans[count], 2);
			tries++;
			HAL_Delay(1);
			// TODO: put some kind of timeout here
		} while(count < 2 && tries < 1000);

		if(strncmp((char *)ans, "OK", 2) != 0) {
			dbg_err("ans=%s\r\n", ans);
			ret = SIGFOX_ERR;
			goto exit;
		}

	} else {
		char *token;
		int i;
		/* The downlink contains a HEX string separated by spaces
		 * e.g.: RX=00 00 30 6A 00 00 FF AD */

		if(strncmp((char *)ans, "RX", 2) != 0) {
			dbg_err("ans=%s\r\n", ans);
			ret = SIGFOX_ERR;
			goto exit;
		}

		uart_read_string(&sigfox_uart, (char *)ans, sizeof(ans));

		for(i = 0, token = strtok((char *)ans, " "); token != NULL;
				i++, token = strtok(NULL, " ")) {
			unsigned int v;
			/* Skips the RX token that the message begins */
			if(strncmp(token, "RX", 2) == 0) {
				/* The first token is of the format RX=xx */
				token = &token[3];
			}
			sscanf(token, "%x", &v);
			msg->rx_msg[i] = (v & 0xFF);
		}
		msg->rx_msg_size = i;
	}

	exit:
	return ret;
}

sigfox_status_e sigfox_get_DeviceID(uint8_t *devid, size_t *devid_size)
{
	sigfox_status_e ret = SIGFOX_OK;

	sigfox_cleanup();

	uart_printf(&sigfox_uart, "AT$I=10\r\n");
	while(HAL_UART_GetState(&sigfox_uart) == HAL_UART_STATE_BUSY_TX);
	*devid_size = uart_read_string(&sigfox_uart, (char *)ans, sizeof(ans));

	memcpy(devid, ans, *devid_size);

	return ret;
}

sigfox_status_e sigfox_get_PAC(uint8_t *pac, size_t *pac_size)
{
	sigfox_status_e ret = SIGFOX_OK;

	sigfox_cleanup();

	uart_printf(&sigfox_uart, "AT$I=11\r\n");
	while(HAL_UART_GetState(&sigfox_uart) == HAL_UART_STATE_BUSY_TX);
	*pac_size = uart_read_string(&sigfox_uart, (char *)ans, sizeof(ans));

	memcpy(pac, ans, *pac_size);

	return ret;
}

sigfox_status_e sigfox_msg_set_tx_msg(sigfox_msg_t *msg,
		const void *msg_data, uint8_t msg_size)
{
	sigfox_status_e ret = SIGFOX_OK;

	if(msg_size > SIGFOX_MAX_PAYLOAD) {
		ret = SIGFOX_INVALID_PAYLOAD;
		goto exit;
	}

	memcpy(msg->tx_msg, msg_data, msg_size);
	msg->tx_msg_size = msg_size;

	exit:
	return ret;
}

void sigfox_msg_enable_downlink(sigfox_msg_t *msg)
{
	msg->request_downlink = DOWNLINK_TRUE;
}

void sigfox_msg_disable_downlink(sigfox_msg_t *msg)
{
	msg->request_downlink = DOWNLINK_FALSE;
}

int sigfox_is_present(void)
{
	return sigfox_is_alive() == SIGFOX_OK;
}
