/*
 * comm.c
 *
 *  Created on: Dec 22, 2017
 *      Author: czr@cesar.org.br
 */

#include "comm/comm.h"
#include "sigfox/sigfox.h"
#include "dbg/dbg.h"
#include "util/crc8.h"
#include "hw/sensors.h"

#include <string.h>

#define MSG_QUEUE_SIZE	10
static uint8_t msgCounter = 0;
static atas_msg_t queue[MSG_QUEUE_SIZE];
static int q_head = 0, q_tail = 0;

static int queue_get_next_msg(atas_msg_t *msg)
{
	if(q_head == q_tail) {
		return 0;	// Queue empty
	}

	memcpy(msg, &queue[q_head], sizeof(atas_msg_t));
	q_head++;
	if(q_head == MSG_QUEUE_SIZE)	q_head = 0;

	return 1;
}

static int queue_put_msg(const atas_msg_t *msg)
{
	if((q_tail + 1) % MSG_QUEUE_SIZE == q_head) {
		/* Discards the oldest message if queue gets full */
		q_head += 1;
		q_head = q_head % MSG_QUEUE_SIZE;
	}

	memcpy(&queue[q_tail], msg, sizeof(atas_msg_t));
	q_tail++;
	if(q_tail == MSG_QUEUE_SIZE)	q_tail = 0;

	return 1;
}

static int queue_size(void)
{
	if(q_tail == q_head)
		return 0;
	else if(q_tail > q_head)
		return q_tail - q_head + 1;
	else
		return MSG_QUEUE_SIZE - 1 - q_head + q_tail;
}

static comm_status_e comm_send_msg(int msg_type, int downlink)
{
	comm_status_e ret = COMM_OK;
	atas_msg_t msg;
	sigfox_msg_t sigfox_msg;

	memset(&msg, 0x00, sizeof(msg));
	memset(&sigfox_msg, 0x00, sizeof(sigfox_msg));

	msg.msgType = msg_type;
	msg.msgCounter = msgCounter++;
	for(int i = 0; i < 4; i++) {
		sensors_read(i, &msg.sensors[i]);
		/* Revert data to big endian - simplifies interpretation */
		msg.sensors[i].sensor_16 = REV16(msg.sensors[i].sensor_16);
	}
	
	msg.msgChecksum = crc8_calc(&msg, sizeof(msg) - sizeof(msg.msgChecksum));

	if(queue_put_msg(&msg) == 0) {
		ret = COMM_ERR;
		goto exit;
	}

	exit:
	return ret;
}

comm_status_e comm_send_periodic_msg(void)
{
#ifdef BOARD_SIGFOX
	return comm_send_msg(MSG_TYPE_PERIODIC_MSG, DOWNLINK_FALSE);
#else
	return COMM_ERR;
#endif
}

comm_status_e comm_send_low_batt_msg(void)
{
#ifdef BOARD_SIGFOX
	return comm_send_msg(MSG_TYPE_LOW_BATTERY_MSG, DOWNLINK_FALSE);
#else
	return COMM_ERR;
#endif
}

comm_status_e comm_send_sensor_variation_msg(uint8_t msgType)
{
#ifdef BOARD_SIGFOX
	return comm_send_msg(msgType, DOWNLINK_FALSE);
#else
	return COMM_ERR;
#endif
}

comm_status_e comm_send_queued_messages(void)
{
	comm_status_e ret = COMM_OK;

#ifdef BOARD_SIGFOX
	atas_msg_t msg;
	sigfox_msg_t sigfox_msg;
	sigfox_status_e st;

	if(queue_get_next_msg(&msg) == 0) {
		ret = COMM_OK;
		goto exit;
	}

	st = sigfox_msg_set_tx_msg(&sigfox_msg, &msg, sizeof(msg));
	if(st != SIGFOX_OK) {
		ret = COMM_ERR;
		goto exit;
	}

	sigfox_msg_disable_downlink(&sigfox_msg);
	st = sigfox_send_frame(&sigfox_msg);

	if(st != SIGFOX_OK) {
		dbg_err("%s:%d: sigfox_send_frame: %s\r\n", __FILE__, __LINE__,
				sigfox_status_str(st));
		ret = COMM_ERR;
		goto exit;
	}
#else
	ret = COMM_ERR;
#endif

	exit:
	return ret;
}
