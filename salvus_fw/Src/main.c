#include "hw/hw_init.h"
#include "hw/adc.h"
#include "hw/leds.h"
#include "hw/power.h"
#include "hw/hw_autotest.h"
#include "hw/batt.h"
#include "hw/sensors.h"
#include "hw/gpio.h"
#include "comm/comm.h"
#include "util/crc8.h"
#include "usb_device.h"
#include "dbg/dbg.h"
#ifdef BOARD_SIGFOX
#include "sigfox/sigfox.h"
#endif
#include <stdlib.h>

#define ONE_MINUTE	2
#define N_MINUTES(n) (n*ONE_MINUTE)
#define USB_POWER_SLEEP_TIME	30000

static void hw_start(void);
static void flow_sensor_testing(void);

int main(void)
{
#ifdef BOARD_SIGFOX
	sigfox_status_e st;
#endif
	uint32_t awake_count, tick_count;
	adc_calibration_t calib;
	hw_autotest_e autotest_st;
	int send_low_bat_msg = 0;
	int last_flux = 0;
	comm_status_e comm_st;
	hw_start();
	

	
	dbg_log("\r\nHardware initialization done!\r\n");
	dbg_log("Build date: %s, %s\r\n", __DATE__, __TIME__);

	/* Run autotest routine */
	dbg_log("Running autotest...\r\n");
	leds_alive_set(LEDS_ON);
	/*Enabling sensor power*/
	P5V0_EN (HIGH);
	HAL_Delay(500);
	adc_start_conversion();
	HAL_Delay(500); // Timing to allow ADC ring buffer to fill up
	adc_pressure_calibrate(&calib);
	autotest_st = hw_run_autotest();
	leds_alive_set(LEDS_OFF);

	if(autotest_st != AUTOTEST_SUCCESS)	{
		dbg_err("Autotesting failed: [%s]\r\n", hw_autotest_str(autotest_st));
		hw_autotest_fail(autotest_st);
	}

	/* Init everything else... */
	crc8_table_populate();
	dbg_log("Value calibrated: pres=%u, diff_pres=%u\r\n",
			calib.adc_pressure, calib.adc_diff_pressure);

#ifdef BOARD_SIGFOX
	st = sigfox_init();
	dbg_debug("sigfox_init(): %s\r\n", sigfox_status_str(st));
	if(st != SIGFOX_OK) hw_autotest_fail(AUTOTEST_COMM_FAIL);

	/* Prints SIGFOX information to USB port */
	sigfox_print_info();
#endif
	
	awake_count = 0;
	while(1)
	{		
		leds_e leds_state = LEDS_ALIVE;
		tick_count = HAL_GetTick();

		/* Check pressure level */
		sensor_t pressure_sensor;
		sensors_read(SENSOR_TYPE_PRESSURE, &pressure_sensor);
		if(pressure_sensor.sensor.sensorValue < PRESSURE_LOW_VALUE_THRESHOLD &&
			pressure_sensor.sensor.sensorValue > 0) {
			leds_state |= LEDS_LOW_PRESSURE;
		} else {
			leds_state &= ~LEDS_LOW_PRESSURE;
		}

		/* Check O2 flux */
		sensor_t diff_pressure_sensor;
		sensors_read(SENSOR_TYPE_DIFF_PRESSURE, &diff_pressure_sensor);
		int flux = (int)diff_pressure_sensor.sensor.sensorValue;
		if(last_flux == 0 && flux > 0 && abs(flux - last_flux) > 8) {
			// Turning on flux
			dbg_debug("Sending flux_on_msg... ");
			comm_st = comm_send_sensor_variation_msg(MSG_TYPE_AIR_FLUX_ON);
			if(comm_st == COMM_OK) {
				dbg_printf("OK\r\n");
			} else {
				dbg_printf("Message with probem\r\n");
			}
		} else if (last_flux > 0 && flux == 0 && abs(flux - last_flux) > 8) {
			// Turning off flux
			dbg_debug("Sending flux_off_msg... ");
			comm_st = comm_send_sensor_variation_msg(MSG_TYPE_AIR_FLUX_OFF);
			if(comm_st == COMM_OK) {
				dbg_printf("OK\r\n");
			} else {
				dbg_printf("Message with probem\r\n");
			}
		} else if (abs(flux - last_flux) > DIFF_PRESSURE_VARIATION_THRESHOLD) {
			dbg_debug("Sending flux_variation_msg... ");
			comm_st = comm_send_sensor_variation_msg(MSG_TYPE_FLUX_VARIATION_MSG);
			if(comm_st == COMM_OK) {
				dbg_printf("OK\r\n");
			} else {
				dbg_printf("Message with probem\r\n");
			}
		}
		last_flux = flux;

		if(awake_count && awake_count % N_MINUTES(2) == 0) {
			/* A minute has gone by... send periodic message */
			dbg_debug("Sending message... ");

			comm_st = comm_send_periodic_msg();
			if(comm_st == COMM_OK) {
				dbg_printf("OK\r\n");
			} else {
				dbg_printf("Message with probem\r\n");
			}
		}

		/* Check battery level */
		if(input_powerpath_status() == POWERPATH_VBAT) 
		{
			batt_voltage_e batt_st = batt_check_voltage();
			switch(batt_st) {
				case BATT_NORMAL_VOLTAGE:
					leds_state &= ~LEDS_LOW_BATT;
					send_low_bat_msg = 0;
					break;

				case BATT_MSG_LOW_VOLTAGE:
				case BATT_LED_LOW_VOLTAGE:
					if (batt_st == BATT_LED_LOW_VOLTAGE)
						leds_state |= LEDS_LOW_BATT;
					else
						leds_state &= ~LEDS_LOW_BATT;

					if(!send_low_bat_msg) {
						send_low_bat_msg = 1;
						dbg_debug("Sending low_batt_msg... ");
						comm_st = comm_send_low_batt_msg();
						if(comm_st == COMM_OK) {
							dbg_printf("OK\r\n");
						} else {
							dbg_printf("Message with problem\r\n");
						}
					}
					break;

				default:
					leds_state &= ~LEDS_LOW_BATT;
					break;
			}
		}

		/* Blinks leds */
		leds_quick_blink(leds_state);

		/* don't send messages if batt is low: can kill the SIGFOX module */
		if((input_powerpath_status() == POWERPATH_VBAT &&
			batt_check_voltage() != BATT_LED_LOW_VOLTAGE) ||
			input_powerpath_status() == POWERPATH_USB)
		{
		
			comm_send_queued_messages();
		}

		/* Done what should be done and now must rest... */
		if(power_usb_is_connected() == USB_OFFLINE) {
			power_switch_to_sleep_mode();
			power_switch_to_run_mode();

		} else {
			/* FIXME: *must* correct this if on USB */
			uint32_t tick = HAL_GetTick();
			HAL_Delay(USB_POWER_SLEEP_TIME -  (tick - tick_count));
		}

		awake_count++;
	}
}

static void hw_start(void)
{
	/* Reset of all peripherals, Initializes the Flash interface and
	 * the Systick. */
	HAL_Init();

	/* Configure the system clock */
	SystemClock_Config();

	/* Initialize all configured peripherals */
	MX_GPIO_Init();		//Aqui deve-se retirar a comunicação SPI
	MX_USART1_UART_Init();		// debug
	MX_LPUART1_UART_Init();	// SIGFOX
	MX_RTC_Init();
	MX_DMA_Init();
	MX_ADC_Init();
#ifdef DEBUG_CDC
	MX_USB_DEVICE_Init();
#endif

	/* Turning off LEDs */
	leds_alive_set(LEDS_OFF);
	leds_low_pressure_set(LEDS_OFF);
	leds_low_batt_set(LEDS_OFF);



	/* Enabling SIGFOX MODEMS */
#ifdef BOARD_SIGFOX	
	/*Enabling sigfox power*/
	P3V0_RF_EN (HIGH);
	HAL_Delay(500);
	output_sigfox_wakeup(HIGH);
	output_sigfox_reset(HIGH);
#endif

}

static void flow_sensor_testing(void)
{
	uint16_t v = adc_pressure_read_pressure();
	uint16_t dp = adc_diff_pressure_read_pressure();
	dbg_debug("Pressure reading: %d.%.4d bar\r\n",
			v >> 4, (v & 0x0F) * 625);
	dbg_debug("Diff pressure reading: [%u] %d.%.4d l/min\r\n",
			dp, dp >> 4, (dp & 0x0F)*625);
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
