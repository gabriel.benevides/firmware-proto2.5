/*
 * libc_syscalls.c
 *
 *  Created on: Dec 1, 2017
 *      Author: czr@cesar.org.br
 */

#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#define UNUSED __attribute__((unused))

int _sbrk(UNUSED ptrdiff_t *ptr) {
	errno = ENOSYS;
	return -1;
}

ssize_t _write(UNUSED int fd, UNUSED const void *buf, UNUSED size_t n) {
	errno = EIO;
	return -1;
}

int _close(UNUSED int fd) {
	errno = EIO;
	return -1;
}

int _fstat(UNUSED int fd, UNUSED void *b) {
	errno = ENOSYS;
	return -1;
}

int _isatty(void) {
	errno = ENOTTY;
	return -1;
}

int _lseek(UNUSED int fd, UNUSED off_t offset, UNUSED int whence) {
	errno = ENOSYS;
	return -1;
}

ssize_t _read(UNUSED int fd, UNUSED void *buf, UNUSED size_t n) {
	errno = EIO;
	return -1;
}


pid_t
_getpid (void)
{
  return  1;
}

int
_kill (int pid, int sig)
{
  return -1;
}
