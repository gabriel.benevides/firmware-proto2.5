/*
 * sigfox.h
 *
 *  Created on: Dec 4, 2017
 *      Author: czr@cesar.org.br
 */

#ifndef SIGFOX_SIGFOX_H_
#define SIGFOX_SIGFOX_H_

#include <stdint.h>
#include <stddef.h>

typedef enum {
	SIGFOX_OK = 0,
	SIGFOX_ERR,
	SIGFOX_INVALID_PAYLOAD,
	SIGFOX_ERR_UNKNOWN_PROCESSOR

} sigfox_status_e;

typedef enum {
	DOWNLINK_FALSE = 0,
	DOWNLINK_TRUE
} sigfox_downlink_bool_e;

typedef struct {
	uint8_t tx_msg[12];
	uint8_t tx_msg_size;
	uint8_t request_downlink;
	uint8_t rx_msg[12];
	uint8_t rx_msg_size;
} sigfox_msg_t;

extern sigfox_status_e sigfox_is_alive(void);
extern sigfox_status_e sigfox_init(void);
extern sigfox_status_e sigfox_send_frame(sigfox_msg_t *msg);

extern sigfox_status_e sigfox_msg_set_tx_msg(sigfox_msg_t *msg,
		const void *msg_data, uint8_t msg_size);
extern void sigfox_msg_enable_downlink(sigfox_msg_t *msg);
extern void sigfox_msg_disable_downlink(sigfox_msg_t *msg);

extern sigfox_status_e sigfox_get_DeviceID(uint8_t *devid, size_t *devid_size);
extern sigfox_status_e sigfox_get_PAC(uint8_t *pac, size_t *pac_size);

extern int sigfox_is_present(void);

extern sigfox_status_e sigfox_print_info(void);
extern const char *sigfox_status_str(sigfox_status_e st);

#endif /* SIGFOX_SIGFOX_H_ */
