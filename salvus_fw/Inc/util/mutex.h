/*
 * mutex.h
 *
 *  Created on: Dec 4, 2017
 *      Author: czr@cesar.org.br
 */

#ifndef UTIL_MUTEX_H_
#define UTIL_MUTEX_H_

typedef struct {
	int lock;
} mutex_t;

#define MUTEX_INIT {0}

typedef enum {
	MUTEX_ERROR_LOCK	= -1,
	MUTEX_OK			= 0,
} mutex_status_e;

#define MUTEX_FREE	0
#define MUTEX_LOCK	!MUTEX_FREE

extern void mutex_lock(mutex_t *mutex);
extern void mutex_free(mutex_t *mutex);

extern mutex_status_e mutex_try_lock(mutex_t *mutex);

extern void mutex_init(mutex_t *mutex);

#endif /* UTIL_MUTEX_H_ */
