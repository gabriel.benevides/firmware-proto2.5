/*
 * crc8.h
 *
 *  Created on: Jan 2, 2018
 *      Author: czr
 */

#ifndef UTIL_CRC8_H_
#define UTIL_CRC8_H_

#include <stdint.h>

extern void crc8_table_populate(void);

extern uint8_t crc8_calc(const void *data, uint32_t data_len);

#endif /* UTIL_CRC8_H_ */
