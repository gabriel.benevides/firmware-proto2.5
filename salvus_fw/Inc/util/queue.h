/*
 * queue.h
 *
 *  Created on: Dec 1, 2017
 *      Author: czr@cesar.org.br
 */

#ifndef QUEUE_H_
#define QUEUE_H_

#include <stddef.h>
#include "util/mutex.h"

typedef struct {
	mutex_t mutex;
	int head_idx;
	int tail_idx;
	size_t data_size;
	void *data;
} queue_t;

extern void queue_init(queue_t *q, void *buf, size_t buf_size);

extern void queue_insert(queue_t *q, const void *buf, size_t num);
extern void queue_pop(queue_t *q, void *buf, size_t num);

extern size_t queue_insert_nonblock(queue_t *q, const void *buf, size_t num);
extern size_t queue_pop_nonblock(queue_t *q, void *buf, size_t num);

extern size_t queue_insert_isr(queue_t *q, const void *buf, size_t num);
extern size_t queue_pop_isr(queue_t *q, void *buf, size_t num);

extern size_t queue_count(queue_t *q);
extern size_t queue_count_isr(queue_t *q);

extern int queue_is_empty(queue_t *q);
extern int queue_is_full(queue_t *q);

extern void queue_reset(queue_t *q);

#endif /* QUEUE_H_ */
