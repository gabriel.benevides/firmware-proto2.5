/*
 * uptime.h
 *
 *  Created on: Feb 16, 2018
 *      Author: czr@cesar.org.br
 */

#ifndef UTIL_UPTIME_H_
#define UTIL_UPTIME_H_

#include <time.h>

extern int uptime_get_uptime(struct timespec *up);

extern int uptime_timecmp(const struct timespec *t1, const struct timespec *t2);

#endif /* UTIL_UPTIME_H_ */
