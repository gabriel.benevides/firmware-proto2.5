/*
 * assert.h
 *
 *  Created on: Dec 1, 2017
 *      Author: czr@cesar.org.br
 */

#ifndef DBG_ASSERT_H_
#define DBG_ASSERT_H_

#if USE_FULL_ASSERT

extern void assert_failed(uint8_t* file, uint32_t line);

#else
#define assert_failed(file, line)	(void)0
#endif

#endif /* DBG_ASSERT_H_ */
