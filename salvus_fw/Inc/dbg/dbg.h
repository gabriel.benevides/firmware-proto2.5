/*
 * dbg.h
 *
 *  Created on: Dec 1, 2017
 *      Author: czr@cesar.org.br
 */

#ifndef DBG_DBG_H_
#define DBG_DBG_H_

#include <stdarg.h>

extern int dbg_printf(const char *fmt, ...);

extern int dbg_vprintf(const char *fmt, va_list args);

extern int dbg_debug(const char *fmt, ...);
extern int dbg_info(const char *fmt, ...);
extern int dbg_warn(const char *fmt, ...);
extern int dbg_err(const char *fmt, ...);

extern int dbg_log(const char *fmt, ...);

#endif /* DBG_DBG_H_ */
