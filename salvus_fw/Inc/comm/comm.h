/*
 * comm.h
 *
 *  Created on: Dec 22, 2017
 *      Author: czr@cesar.org.br
 */

#ifndef COMM_COMM_H_
#define COMM_COMM_H_

#include "hw/sensors.h"

#include <stdint.h>

typedef struct {
	uint8_t msgType;
	uint8_t msgCounter;
	sensor_t sensors[4];
	uint8_t msgChecksum;
} __attribute__((packed)) atas_msg_t;

#define MSG_TYPE_PERIODIC_MSG				0x00
#define MSG_TYPE_LOW_BATTERY_MSG			0x01
#define MSG_TYPE_TAMPER_MSG				0x02
#define MSG_TYPE_AIR_FLUX_OFF				0x03
#define MSG_TYPE_AIR_FLUX_ON				0x04
#define MSG_TYPE_FLUX_VARIATION_MSG			0x05

typedef enum {
	COMM_OK = 0,
	COMM_ERR
} comm_status_e;

extern comm_status_e comm_send_periodic_msg(void);
extern comm_status_e comm_send_low_batt_msg(void);
extern comm_status_e comm_send_sensor_variation_msg(uint8_t msgType);
extern comm_status_e comm_send_queued_messages(void);

#define REV16(x) ((x)>>8|(x)<<8)

#endif /* COMM_COMM_H_ */
