/*
 * hw_pin_defs.h
 *
 *  Created on: Mar 21, 2018
 *      Author: czr@cesar.org.br
 */

#ifndef HW_HW_PIN_DEFS_H_
#define HW_HW_PIN_DEFS_H_

#include <stm32l0xx_hal_gpio.h>

/* LEDS */
#define LEDS_USER_LED1_Port 	        GPIOB
#define LEDS_USER_LED1_Pin		GPIO_PIN_3
#define LEDS_USER_LED2_Port 	        GPIOB
#define LEDS_USER_LED2_Pin             GPIO_PIN_4
#define LEDS_USER_LED3_Port		GPIOB
#define LEDS_USER_LED3_Pin 		GPIO_PIN_5

#define LEDS_LOW_BAT_Port 		GPIOB
#define LEDS_LOW_BAT_Pin		GPIO_PIN_3
#define LEDS_LOW_PRESSURE_Port 	GPIOB
#define LEDS_LOW_PRESSURE_Pin	        GPIO_PIN_4
#define LEDS_HEARTBEAT_Port		GPIOB
#define LEDS_HEARTBEAT_Pin 		GPIO_PIN_5

/* ADC for pressure and flow */
#define ADC_PRESSURE_VOUT_Port	GPIOA
#define ADC_PRESSURE_VOUT_Pin	        GPIO_PIN_4
#define ADC_FLOW_VOUT_Port		GPIOA
#define ADC_FLOW_VOUT_Pin		GPIO_PIN_5

/* SIGFOX */
#define SIGFOX_Reset_Port   	 GPIOB
#define SIGFOX_Reset_Pin        GPIO_PIN_8
#define SIGFOX_Wakeup_Port      GPIOB
#define SIGFOX_Wakeup_Pin       GPIO_PIN_9
#define SIGFOX_TX_Port          GPIOB
#define SIGFOX_TX_Pin           GPIO_PIN_10
#define SIGFOX_RX_Port          GPIOB
#define SIGFOX_RX_Pin           GPIO_PIN_11

/* SARA-350 */
#define SARA_CTS_Port           GPIOA
#define SARA_CTS_Pin            GPIO_PIN_0
#define SARA_RTS_Port           GPIOA
#define SARA_RTS_Pin            GPIO_PIN_1
#define SARA_TX_Port            GPIOA
#define SARA_TX_Pin             GPIO_PIN_2
#define SARA_RX_Port            GPIOA
#define SARA_RX_Pin             GPIO_PIN_3
#define SARA_RESET_N_Port       GPIOA
#define SARA_RESET_N_Pin        GPIO_PIN_6
#define SARA_PWR_ON_Port        GPIOA
#define SARA_PWR_ON_Pin         GPIO_PIN_7

/* Power-associated pins */
#define POWER_MCU_PP_STAT1_Port GPIOA
#define POWER_MCU_PP_STAT1_Pin  GPIO_PIN_8
#define POWER_P3V_GOOD_Port     GPIOA
#define POWER_P3V_GOOD_Pin      GPIO_PIN_15
#define MISC_PVBAT_ADC_Port     GPIOB
#define MISC_PVBAT_ADC_Pin      GPIO_PIN_1

/* MISC Pins */
//SUBSTITUIR O NOME DO PINO 0 E DO PINO 1 PELAS LEITURAS QUE ELES FAZEM DO CONV DC DC E DO REGULADORs
#define MISC_MCU_ADC_IN8_Port   GPIOB
#define MISC_MCU_ADC_IN8_Pin    GPIO_PIN_0
#define MISC_MCU_ADC_IN9_Port   GPIOB
#define MISC_MCU_ADC_IN9_Pin    GPIO_PIN_1
#define MISC_USER_BUTTON_Port   GPIOB
#define MISC_USER_BUTTON_Pin    GPIO_PIN_12
#define MISC_P3V0_RF_EN_Port    GPIOB
#define MISC_P3V0_RF_EN_Pin     GPIO_PIN_14
#define MISC_P5V0_EN_Port    	 GPIOB
#define MISC_P5V0_EN_Pin     	 GPIO_PIN_15

#endif /* HW_HW_PIN_DEFS_H_ */
