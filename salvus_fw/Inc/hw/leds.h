/*
 * leds.h
 *
 *  Created on: Feb 20, 2018
 *      Author: czr@cesar.org.br
 */

#ifndef HW_LEDS_H_
#define HW_LEDS_H_

typedef enum {
	LEDS_OFF = 0,
	LEDS_ON,
} leds_state_e;

typedef enum {
	LEDS_ALIVE			= (1 << 0),
	LEDS_LOW_PRESSURE	= (1 << 1),
	LEDS_LOW_BATT		= (1 << 2)
} leds_e;

extern void leds_alive_set(leds_state_e val);

extern void leds_low_batt_set(leds_state_e val);

extern void leds_low_pressure_set(leds_state_e val);

extern void leds_quick_blink(leds_e leds);

#endif /* HW_LEDS_H_ */
