/*
 * hw_init.h
 *
 *  Created on: Dec 1, 2017
 *      Author: czr@cesar.org.br
 */

#ifndef HW_HW_INIT_H_
#define HW_HW_INIT_H_

#include "stm32l0xx_hal.h"
#include "stm32l0xx_hal_dma.h"
#include "stm32l0xx_hal_uart.h"
#include "stm32l0xx_hal_rtc.h"
#include "stm32l0xx_hal_adc.h"
#include "stm32l0xx_hal_dma.h"

#include "util/queue.h"

extern void SystemClock_Config(void);
extern void MX_GPIO_Init(void);
extern void MX_USART1_UART_Init(void);
extern void MX_USART2_UART_Init(void);
extern void MX_LPUART1_UART_Init(void);
extern void MX_RTC_Init(void);
extern void MX_ADC_Init(void);
extern void MX_DMA_Init(void);

extern void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)

extern UART_HandleTypeDef sigfox_uart;
extern UART_HandleTypeDef debug_uart;
extern UART_HandleTypeDef gprs_uart;

extern RTC_HandleTypeDef hrtc;

extern ADC_HandleTypeDef hadc;
extern DMA_HandleTypeDef hdma_adc;

#endif /* HW_HW_INIT_H_ */
