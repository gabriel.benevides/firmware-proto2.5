/*
 * uart.h
 *
 *  Created on: Dec 1, 2017
 *      Author: czr
 */

#ifndef UART_H_
#define UART_H_

#include <stddef.h>
#include "stm32l0xx_hal.h"
#include <stdarg.h>

extern ssize_t uart_write(UART_HandleTypeDef *huart,
		const void *buf, size_t num);

extern ssize_t uart_read(UART_HandleTypeDef *huart,
		void *buf, size_t num);

extern ssize_t uart_read_string(UART_HandleTypeDef *huart,
		char *str, size_t len);

extern int uart_printf(UART_HandleTypeDef *huart,
		const char *fmt, ...);

extern int uart_vprintf(UART_HandleTypeDef *huart,
		const char *fmt, va_list args);

#endif /* UART_H_ */
