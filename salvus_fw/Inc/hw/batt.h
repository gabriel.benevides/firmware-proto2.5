/*
 * batt.h
 *
 *  Created on: Mar 21, 2018
 *      Author: czr@cesar.org.br
 */

#ifndef HW_BATT_H_
#define HW_BATT_H_

#include <stdint.h>

typedef enum {
	BATT_NORMAL_VOLTAGE = 0,
	BATT_MSG_LOW_VOLTAGE,
	BATT_LED_LOW_VOLTAGE
} batt_voltage_e;

extern batt_voltage_e batt_check_voltage(void);

extern uint32_t batt_measure_voltage(void);

#endif /* HW_BATT_H_ */
