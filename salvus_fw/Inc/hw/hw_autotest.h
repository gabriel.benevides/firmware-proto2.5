/*
 * hw_autotest.h
 *
 *  Created on: Mar 20, 2018
 *      Author: czr@cesar.org.br
 */

#ifndef HW_HW_AUTOTEST_H_
#define HW_HW_AUTOTEST_H_

typedef enum {
	AUTOTEST_SUCCESS = 0,
	AUTOTEST_TAMPER_FAIL,
	AUTOTEST_COMM_FAIL,
	AUTOTEST_SENSORS_FAIL,
} hw_autotest_e;

extern hw_autotest_e hw_run_autotest(void);

extern void hw_autotest_fail(hw_autotest_e st);

extern const char *hw_autotest_str(hw_autotest_e st);

#endif /* HW_HW_AUTOTEST_H_ */
