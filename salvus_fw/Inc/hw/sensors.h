/*
 * sensors.h
 *
 *  Created on: Jan 4, 2018
 *      Author: czr
 */

#ifndef HW_SENSORS_H_
#define HW_SENSORS_H_

#include <stdint.h>

typedef enum {
	SENSOR_TYPE_PRESSURE = 0,
	SENSOR_TYPE_DIFF_PRESSURE,
	SENSOR_BATT_MEASUREMENT,
	SENSOR_TYPE_OXIMETER
} sensor_type_e;

/* Little endian format: high order bitfields *MUST* come last */
struct _sensor {
	uint16_t sensorValue:12;
	uint16_t sensorType:4;
} __attribute__((packed));

typedef union {
	struct _sensor sensor;
	uint16_t sensor_16;
} sensor_t;

extern void sensors_read(sensor_type_e sensor_idx, sensor_t *sensor);

#endif /* HW_SENSORS_H_ */
