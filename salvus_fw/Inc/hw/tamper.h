/*
 * tamper.h
 *
 *  Created on: Mar 20, 2018
 *      Author: czr@cesar.org.br
 */

#ifndef HW_TAMPER_H_
#define HW_TAMPER_H_

typedef enum {
	TAMPER_OK = 0,
	TAMPER_VIOLATION
} tamper_status_e;

extern tamper_status_e tamper_get_status(void);

#endif /* HW_TAMPER_H_ */
