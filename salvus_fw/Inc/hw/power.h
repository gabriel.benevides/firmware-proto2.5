/*
 * power.h
 *
 *  Created on: Feb 19, 2018
 *      Author: czr
 */

#ifndef HW_POWER_H_
#define HW_POWER_H_

extern void power_switch_to_sleep_mode(void);

extern void power_switch_to_run_mode(void);

typedef enum {
    USB_OFFLINE = 0,
    USB_ONLINE
} usb_status_e;

extern void power_usb_set_connected(usb_status_e st);

extern usb_status_e power_usb_is_connected(void);

#endif /* HW_POWER_H_ */
