/*
 * adc.h
 *
 *  Created on: Dec 21, 2017
 *      Author: czr
 */

#ifndef HW_ADC_H_
#define HW_ADC_H_

#include <stdint.h>

typedef struct {
	uint32_t adc_pressure;
	uint32_t adc_diff_pressure;
} adc_calibration_t;

/*
 * Calibrates the pressure sensor - the calibration routine verifies the base
 * value of the sensor (zero psi) and stores it as zero
 */
extern void adc_pressure_calibrate(adc_calibration_t *calib);

/*
 * Enables the conversion of the ADC hardware
 */
extern void adc_start_conversion(void);

/*
 * Reads a pressure value
 */
extern uint32_t adc_pressure_read_pressure(void);

/*
 * Reads a difference pressure value
 */
extern uint32_t adc_diff_pressure_read_pressure(void);

/*
 * Disables the ADC and stop the conversion
 */
extern void adc_stop_conversion(void);

/*
 * Returns the calibration values
 */
extern void adc_pressure_get_calibration(adc_calibration_t *calib);

#define PRESSURE_LOW_VALUE_THRESHOLD		480	/* 480 = 30 bar */
#define DIFF_PRESSURE_VARIATION_THRESHOLD	16	/* 16 = 1,0 l/min */

/**
 * Reads the PVBAT ADC value
 */
extern uint32_t adc_read_pvbat_value(void);

extern uint32_t adc_read_pvbat_voltage(void);

#endif /* HW_ADC_H_ */
