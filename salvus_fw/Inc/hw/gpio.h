/*
 * input.h
 *
 *  Created on: Apr 26, 2018
 *      Author: czr@cesar.org.br
 */

#ifndef HW_GPIO_H_
#define HW_GPIO_H_

#include <stm32l0xx_hal_gpio.h>

typedef enum {
    LOW = GPIO_PIN_RESET,
    HIGH = GPIO_PIN_SET
} gpio_e;

extern gpio_e input_read_user_button(void);

extern gpio_e input_read_power_good(void);

typedef enum {
    POWERPATH_VBAT,
    POWERPATH_USB
} powerpath_e;

extern powerpath_e input_powerpath_status(void);

extern void output_sigfox_reset(gpio_e st);

extern void output_sigfox_wakeup(gpio_e st);

extern void output_sara_reset_n(gpio_e st);

extern void output_sara_pwr_on(gpio_e st);

extern void P3V0_RF_EN (gpio_e st);

extern void P5V0_EN (gpio_e st);
#endif
