# README file
This file explains a little how to build and run this project.

## Building process
This project was written using Fedora 27 and 28. The following packages must be installed
  * `arm-none-eabi-gcc-cs`
  * `arm-none-eabi-newlib`
  * `stlink` (for st-flash application)

Plase make sure that [RPM Fusion](https://rpmfusion.org/) repositories are installed and enabled in your machine. RPM Fusion website explains how this can be accomplished.

### Debugging facilities

To enable debug you have to add `-DUSE_DEBUG` to the `DEFINES` rule in the Makefile file. The default debug mode is via a USART port of the microcontroller. To enable USB-CDC debugging mode you have to also add `-DDEBUG_CDC` to the `DEFULES` rule of the Makefile.

For the default debug configuration:
> DEFINES += (...) -DUSE_DEBUG

For USB-CDC debug configuration
> DEFINES += (...) -DUSE_DEBUG -DDEBUG_CDC

With these defines you have full access to all debugging functions as defined in the `dbg.h` and `dbg.c` files.

### Choosing a board to build
This project mandates that a hardware version for Sigfox and GPRS should be done. Due to electrical constrains both boards have different vontage levels: Sigfox board runs on a 3VDC voltage whereas the GPRS board runs on a 3.5VDC voltage. Due to this difference a board *must* be specified during compilation time. To specify a board you should either define `-DBOARD_SIGFOR` or `-DBOARD_GPRS` in the `DEFINES` rule of the Makefile:

Sigfox board:
> DEFINES += (...) -DBOARD_SIGFOX

GPRS board:
> DEFINES += (...) -DBOARD_GPRS

## Building the project
To build the project you can type:
> $ make

Or to make things a little faster (if the project is being built on a multi-core CPU)

> $ make -j

## Firmware flashing procedure

There is a rule in the Makefile that allows quick flashing facilities if developing on a Linux-box or another compatible OS and if the st-flash application is available.

After the building process is done you can flash the board by typing
> $ make write

or by typing the command all the way:
> $ st-flash write salvus_fw.bin 0x08000000
